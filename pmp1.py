#!/usr/bin/python
#
# Play PMP GAME By Didier Dubois 
#
#
import json
import random

version = "16.04.08-4"

file = "game.json"

class Question:
    """Helping questionning & validating results """
    sols = [ " ERROR  : %(s)s [%(a)s] : %(q)s" ,
             " OK  : %(s)s : %(q)s" ]
    
    def __init__(self, q, s, keys):
        self.question = q
        self.solution = s
        self.keys = keys
        self.input = "  " + str([k[1] for k in keys]) + " : " 

    def ask(s):
        print("\n" + s.question)
        for k in s.keys:
            print( " " + k[1] +  " - "  + k[0])
        s.answer = input( s.input)        

    def validate(s):
        i = s.count()
        print( s.sols[i] %
               { 's': s.solution, 'a': s.answer, 'q': s.question } ) 

    def count(s):
        if s.answer == s.solution:
            return 1
        return 0

class QuestionsContainer:
    """Container for questions"""
    def __ask__(self, text):        
        print ( text)
        [ q.ask() for q in self.questions ]

    def ask(self):
        self.__ask__("")
        
    def count(self):
        return sum( [ q.count() for q in self.questions] )
    
    def validate(self):
        [ q.validate() for q in self.questions ] 
        return self.count()

class PhaseQuestions(QuestionsContainer):
    """ Questions for Phases"""
    def __init__(self, data, num):
        flat = self.flatten_questions(data)
        self.questions = [ self.contest(flat, data) for _ in range(num) ]
        self.num = num
        self.keys = data

    def flatten_questions(self, data):
        """ Flatten the list of Questions from input data """
        return [ ( elem, quest[0], quest[1] )
             for quest in data
             for elem in quest[2]  ]
    
    def ask(self):
        self.__ask__("\nPlease select the correct phase:")            
    
    def contest(self, data, keys):
        """Build a constest of quetion"""
        index = random.randrange(len(data) - 1)
        qx = data[ index  ]
        return Question(qx[0], qx[2], keys)

def contest(lst_in, question, isPrev):
    """Build a constest of questions"""
    lst = lst_in.copy()
    index = random.randrange(len(lst) - 3) # We do not consider 1st & last
    prv  = lst.pop( index )
    line = lst.pop( index )
    nxt  = lst.pop( index )
    index = random.randrange(len(lst) - 3)
    other1 = lst.pop(index)
    index = random.randrange(len(lst) - 3)
    other2 = lst.pop(index)

    if isPrev: answer = prv
    else: answer = nxt
    
    values = [ prv, nxt, other1, other2 ]
    random.shuffle( values )
    
    ques = question % (line)
    propal = [ (values[i], str(i)) for i in range(len(values))  ]        
    solx = [ val[1] for val in propal if val[0] == answer  ]
    return Question(ques, solx[0], propal )

class PlanningFollowingQuestions(QuestionsContainer):
    """ Questions for Planning"""
    def __init__(self, data, num):
        QuestionsContainer.__init__(self)
        
        question = "What is following: '%s' ?"
        isPrev = False
        
        v = [ d for d in data if d[0] == 'Planning' ][0]
        lst = ["None" ] + v[2] + ["None"]
        self.questions =  [ contest(lst, question, isPrev) for _ in range(num) ]
        self.num = num

class PlanningPreviousQuestions(QuestionsContainer):
    """ Questions for Planning"""
    def __init__(self, data, num):
        QuestionsContainer.__init__(self)
        
        question = "What is before: '%s' ?"
        isPrev = True
        
        v = [ d for d in data if d[0] == 'Planning' ][0]
        lst = ["None" ] + v[2] + ["None"]
        self.questions =  [ contest(lst, question, isPrev) for _ in range(num) ]
        self.num = num

def load(f):
    """Load Json File with questions."""
    with open(file, 'r') as f:
    	data = json.load(f)

    return [ (k, k[0].upper(), data[k]) for k in data.keys() ]


## Building questions
data = load(file)

num = 4
questions =  [
            PhaseQuestions(data, num ),
            PhaseQuestions(data, num ),
            PlanningFollowingQuestions(data, num),
            PlanningPreviousQuestions(data, num)
            ]

for q in questions:
    q.ask()


print("\n\nYour Results:\n")
okies = sum( [ q.validate() for q in questions]  )               
total = num * len(questions)

print( "\nTOTAL: %d/%d : %.0f%% " % ( okies, total, okies/total * 100.) )


